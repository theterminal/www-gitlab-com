require 'yaml'

module Categories
  class CategoriesYmlLinkChecker
    def run(categories_yml_path = nil)
      log "Running categories.yml Link Checker..."

      categories = load_categories_yml(categories_yml_path)
      bad_category_messages = []

      categories.each do |_, category|
        next unless category['maturity'] && category['marketing']

        if category['maturity'] != 'planned' && !category['documentation']
          msg = "Bad Category: '#{category['name']}' at '#{category['maturity']}' maturity with no documentation link."
          bad_category_messages << msg if msg
        end
      end

      log "\n"
      if bad_category_messages.empty?
        log "SUCCESS: categories.yml links are valid, no bad categories found."
      else
        err_msg = "ERROR: categories.yml had #{bad_category_messages.length} bad categories with invalid links"
        log "#{err_msg}:\n"
        bad_category_messages.each_with_index do |msg, i|
          log "#{i + 1}. #{msg}"
        end
        log "\n"
        raise err_msg
      end
    end

    private

    def load_categories_yml(categories_yml_path)
      categories_yml_path ||= File.expand_path('../../data/categories.yml', __dir__)
      categories = YAML.load_file(categories_yml_path)
      categories
    end

    def log(msg)
      puts msg
    end
  end
end
